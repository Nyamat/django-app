from django.shortcuts import render


def home(request):
    return render(request, 'home.html')

def modules(request):
    return render(request, 'modules.html')

def module_lesson(request, module):
    return render(request, f'{module}.html')
