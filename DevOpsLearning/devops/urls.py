from django.urls import path
from . import views

app_name = 'devops'

urlpatterns = [
    path('', views.home, name='home'),  # Add this line for the root path
    path('modules/', views.modules, name='modules'),
    path('module/<str:module>/', views.module_lesson, name='module_lesson'),
    path('home/', views.home, name='home'),
]
